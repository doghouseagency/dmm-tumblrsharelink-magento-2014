# Tumblr Share Link

## API

### Helper

#### generateUrl

Usage:

    $url = Mage::helper('tumblrsharelink')->generateUrl(...);

The parameters are:

- [String] URL to page or photo that is being shared (optional). If this isn't specified, the module will grab the current URL. In the case of a photo, this value is necessary.
- [Array] Associative array with extra options. Possible values are `name` and `description` if a link is being shared, or `clickthru` and `caption` if a photo is being shared.
- [Bool] Specifies whether the URL is a link of photo (Tumblr handles both of these differently).

You can then use the `$url` in your HTML:

    <a href="<?php echo $url ?>" target="_blank">Share on Tumblr!</a>

### Product Helper

Usage:

    $helper = Mage::helper('tumblrsharelink/product')->init($_product);

    <a href="<?php echo $helper ?>" target="_blank">Share product on Tumblr!</a>

The helper has a magic `__toString` method which returns the URL. The product helper puts
the product name and short description in the Tumblr caption and generates a 750 x 750 image of the product and shares the photo along with the caption.

## Testing

Keep in mind sharing photos from a proxy or your developent environment most probably won't work  as the Tumblr servers don't have access.