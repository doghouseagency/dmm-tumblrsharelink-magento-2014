<?php

class Doghouse_TumblrShareLink_Helper_Product extends Doghouse_TumblrShareLink_Helper_Data {

    public function init(Mage_Catalog_Model_Product $product) {
        $this->_product = $product;
        return $this;
    }

    public function __toString()
    {

        $p = $this->_product;

        $caption = sprintf(
            '<h1>%s</h1>%c%s',
            Mage::helper('catalog/output')
            ->productAttribute($p, $p->getName(), 'name'),
            10,
            Mage::helper('catalog/output')
                ->productAttribute($p, $p->getShortDescription(), 'short_descripiton')
        );

        $parms = array(
            'source' => (string) Mage::helper('catalog/image')
                ->init($p, 'image')->resize(750, 750),

            'caption' => $caption
        );

        $shareurl = $p->getProductUrl();

        return $this->generateUrl($shareurl, $parms, true);

    }

}