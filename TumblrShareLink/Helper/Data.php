<?php

class Doghouse_TumblrShareLink_Helper_Data extends Mage_Core_Helper_Abstract {

    /**
     * @return String current URL
     */
    public function getDefaultShareUrl() {
        return Mage::helper('core/url')->getCurrentUrl();
    }

    /**
     * If shareurl, is empty, it uses the current URL
     *
     * Use it like this:
     *
     * $options = array(
     *     'name'           => $this->getName(),
     *     'description'    => $this->getDescription()
     * );
     *
     * return Mage::helper('tumblrsharelink')->generateUrl(
     *       $this->parseUrl(),
     *       $options,
     *       true // is photo. Keep in mind options will change if it is
     * );
     *
     * @see http://www.tumblr.com/buttons
     *
     * @param  [String] Share url
     * @param  [Array]  $data options array
     * @param  [Bool]   [is photo]
     * @return [String] url
     */
    public function generateUrl($shareurl = null, $data = array(), $isPhoto = false) {

        if(!$shareurl) {
            $shareurl = $this->getDefaultShareUrl();
        }

        if(!$isPhoto) {
            $baseUrl = "http://www.tumblr.com/share/link?url=%s";
        } else {
            $baseUrl = "http://www.tumblr.com/share/photo?clickthru=%s";
        }

        $url = sprintf($baseUrl, urlencode($shareurl));

        if(is_array($data)) {

            foreach($data as $key => $value) {
                $url .= sprintf("&amp;%s=%s",
                    urlencode($key),
                    urlencode($value)
                );
            }

        }

        return $url;
    }

}